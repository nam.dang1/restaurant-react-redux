export const ADD_USER = 'ADD_USER';
export const REGISTER_SUCCEES = 'REGISTER_SUCCEES';
export const REGISTER_FAILED = 'REGISTER_FAILED';


export const LOGIN = "LOGIN";
export const LOGIN_FAILED = "LOGIN_FAILED"
export const LOGIN_SUCCEES = 'LOGIN_SUCCEES';
export const ADD_FOOD = "ADD_FOOD";


export const GET_ALL_FOOD = "GET_ALL_FOOD";
export const GET_ALL_FOOD_FAILED = "GET_ALL_FOOD_FAILED";