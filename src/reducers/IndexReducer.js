import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import FoodReducer from './FoodReducer'
const rootReducer = combineReducers({
    AuthReducer,
    FoodReducer
});

export default rootReducer;