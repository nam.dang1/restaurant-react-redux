import * as types from './../constants/ActionTypes'

let initialState = {
    name: "",
    password: ""
};
export default function AuthReducres(state = initialState, action) {

    switch (action.type) {
        case types.ADD_USER:

            return {
                ...state,
                name: action.data.name,
                password: action.data.password
            }

        case types.REGISTER_FAILED:

            return [...state];

        case types.LOGIN:
            localStorage['userName'] = JSON.stringify(action.data.name);
            return {
                ...state,
                user: action.data
            }
        default:
            return state;
    }
}