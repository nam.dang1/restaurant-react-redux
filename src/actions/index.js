import * as types from './../constants/ActionTypes'
import callApi from './../utils/apiCaller'



export const addUserRequest = (user) => {
    return (dispatch) => {
        callApi('users', 'POST', user).then(response => {
                dispatch({
                    type: types.ADD_USER,
                    data: response.data
                })
            })
            .catch((err) => {
                dispatch({
                    type: types.REGISTER_FAILED,
                    data: err
                })
            })

    }
}
export const login = (userName, password) => {

    return (dispatch) => {
        callApi('users', 'GET', null).then(response => {
                let user = (response.data.find((data) => {
                    return data.name === userName && data.password === password;
                }));
                dispatch({
                    type: types.LOGIN,
                    data: user
                })
            })
            .catch((err) => {
                dispatch({
                    type: types.LOGIN_FAILED,
                    data: err
                })
            })
    }
}

export const getAllFood = () => {
    return (dispatch) => {
        callApi('foods', 'GET', null).then(response => {
                console.log(response.data);
                dispatch({
                    type: types.GET_ALL_FOOD,
                    data: response.data
                })
            })
            .catch((err) => {
                dispatch({
                    type: types.GET_ALL_FOOD_FAILED,
                    data: err
                })
            })
    }
}