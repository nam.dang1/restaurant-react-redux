import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, NavLink, Switch, withRouter} from "react-router-dom";
import HomeComponent from './components/HomeComponent/HomeComponent';
import LoginContainer from './containers/LoginContainer';
import RegisterContainer from './containers/RegisterContainer';

function App() {
  return (
    <div>

      <Switch>
          <Route path="/" exact component={HomeComponent} />
          <Route path="/login" exact component={LoginContainer} />
          <Route path="/register" exact component={RegisterContainer} />
       
         
        
        </Switch>
    </div>
    
  );
}

export default withRouter(App);
