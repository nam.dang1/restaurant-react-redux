import React, { Component } from 'react';
class SignatureFoodComponent extends Component {

    componentWillMount() {
        console.log(this.props.a);
        this.props.getAllFoodRequest();
    }
    render() {
        console.log(this.props);
        return (
            <React.Fragment>
                <div className="title">
                    <h1> Our Signature Dishes</h1>

                </div>
                <div className="blog">

                    <div className="container">
                        <div className="row">
                            <div className="col-lg-6 blog_col">
                                <div className="blog_post">
                                    <div className="blog_post_image_container">
                                        <div className="blog_post_image"><img src="images/blog_1.jpg" alt="https://unsplash.com/@patrick_schneider" /></div>
                                        <div className="blog_post_date"><a href="/#" >Oder Now</a></div>
                                    </div>
                                    <div className="blog_post_content">
                                        <div className="blog_post_title"><a href="/#">Our Nomenee at the Restaurants Awards</a></div>
                                        <div className="blog_post_info">
                                            <ul className="d-flex flex-row align-items-center justify-content-start">
                                                <li>by <a href="/#">George Smith</a></li>
                                                <li>in <a href="/#">Lifestyle</a></li>
                                                <li><a href="/#">2 Comments</a></li>
                                            </ul>
                                        </div>
                                        <div className="blog_post_text">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada lorem maximus mauris scelerisque, at rutrum nulla dictum. Ut ac ligula sapien. Suspendisse cursus faucibus finibus. Ut non justo eleifend, facilisis
                                        nibh ut, interdum odio. Suspendisse potenti.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div className="col-lg-6 blog_col">
                                <div className="blog_post">
                                    <div className="blog_post_image_container">
                                        <div className="blog_post_image"><img src="images/blog_2.jpg" alt="https://unsplash.com/@tashytown" /></div>
                                        <div className="blog_post_date"><a href="/#">Oder now</a></div>
                                    </div>
                                    <div className="blog_post_content">
                                        <div className="blog_post_title"><a href="/#">Recipe of the week</a></div>
                                        <div className="blog_post_info">
                                            <ul className="d-flex flex-row align-items-center justify-content-start">
                                                <li>by <a href="/#">George Smith</a></li>
                                                <li>in <a href="/#">Lifestyle</a></li>
                                                <li><a href="/#">2 Comments</a></li>
                                            </ul>
                                        </div>
                                        <div className="blog_post_text">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada lorem maximus mauris scelerisque, at rutrum nulla dictum. Ut ac ligula sapien. Suspendisse cursus faucibus finibus. Ut non justo eleifend, facilisis
                                        nibh ut, interdum odio. Suspendisse potenti.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>
                        <div className="row load_more_row">
                            <div className="col">
                                <div className="button load_more_button ml-auto mr-auto trans_200"><a href="/#">Load More</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>

        );
    }

}

export default SignatureFoodComponent;