import React, { Component } from 'react';
import { BrowserRouter as Router, Route, NavLink, Switch } from "react-router-dom";

class Header extends Component {
    signOut = () =>{
        localStorage.removeItem('userName');
    }
    render() {
        return (
            <React.Fragment>

                <header className="header">
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                <div className="header_content d-flex flex-row align-items-center justify-content-start">
                                    <div className="logo">
                                        <a href="/#">
                                            <div>The Venue</div>
                                            <div>restaurant</div>
                                        </a>
                                    </div>
                                    <nav className="main_nav">
                                        <ul className="d-flex flex-row align-items-center justify-content-start">
                                            <li><a href="index.html">home</a></li>
                                            <li><a href="about.html">about us</a></li>
                                            <li><a href="menu.html">menu</a></li>
                                            <li><a href="/#">delivery</a></li>
                                            <li><a href="contact.html">contact</a></li>
                                            <li><a href="blog.html">Cart</a></li>
                                        </ul>
                                    </nav>
                                    {localStorage.getItem('userName') &&  <div className="reservations_phone ml-auto"><NavLink to="/#">{localStorage.getItem('userName')} </NavLink></div>}
                                    {!localStorage.getItem('userName') &&<div className="reservations_phone ml-auto"><NavLink to="/login">login</NavLink></div>}
                                    {!localStorage.getItem('userName') &&  <div className="reservations_phone ml-auto"><NavLink to="/register">Sign up</NavLink></div>}
                                    {localStorage.getItem('userName') &&  <div className="reservations_phone ml-auto"><NavLink to="/#" onClick ={this.signOut}>Sign out</NavLink></div>}

                                </div>
                            </div>
                        </div>
                    </div>
                </header>



                <div className="hamburger_bar trans_400 d-flex flex-row align-items-center justify-content-start">
                    <div className="hamburger">
                        <div className="menu_toggle d-flex flex-row align-items-center justify-content-start">
                            <span>menu</span>
                            <div className="hamburger_container">
                                <div className="menu_hamburger">
                                    <div className="line_1 hamburger_lines hamburger_lines1" ></div>
                                    <div className="line_2 hamburger_lines hamburger_lines2" ></div>
                                    <div className="line_3 hamburger_lines hamburger_lines3"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div className="menu trans_800">
                    <div className="menu_content d-flex flex-column align-items-center justify-content-center text-center">
                        <ul>
                            <li><a href="index.html">home</a></li>
                            <li><a href="about.html">about us</a></li>
                            <li><a href="menu.html">menu</a></li>
                            <li><a href="/#">delivery</a></li>
                            <li><a href="contact.html">contact</a></li>
                            <li><a href="blog.html">Cart</a></li>
                        </ul>
                    </div>
                    <div className="reservations_phone ml-auto"><a href="/#">login</a></div>
                    <div className="reservations_phone ml-auto"><a href="/#">Logout</a></div>

                </div>



                <div className="home">
                    <div className="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/about.jpg" data-speed="0.8"></div>
                    <div className="home_container">
                        <div className="container">
                            <div className="row">
                                <div className="col">
                                    <div className="home_content text-center">
                                        <div className="home_subtitle page_subtitle">Max Restautant</div>
                                        <div className="home_title">
                                            <h1>Home</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>

        );
    }

}

export default Header;