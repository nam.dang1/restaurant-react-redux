import React, { Component } from 'react';
import Header from './../block/Header'
import Menu from './../menu/menu';
import SignatureFoodComponent from './../foodsComponent/SignatureFoodComponent';
class HomeComponent extends Component {
    render() {
        return (
            <React.Fragment>
                <Header></Header>
                <Menu></Menu>
                <SignatureFoodComponent></SignatureFoodComponent>
            </React.Fragment>

        );
    }

}

export default HomeComponent;
