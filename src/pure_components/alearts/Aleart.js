import React, { PureComponent } from "react";
import './style.css';
class Alert extends PureComponent {

    render() {
        return (
            <div className="row aleart">
                <div className="col-md-6 offset-md-3">
                    <div className="card card-outline-secondary">
                        <div className="card-header">
                            <h3 className="mb-0"> {this.props.title} </h3>
                        </div>
                        <div className="card-body">
                            <p className="card-text"> {this.props.message} </p>
                        </div>
                        <button type="button" className="btn btn-large btn-block btn-primary" onClick={() => {
                            this.props.onOK && this.props.onOK();
                        }}
                        >OK</button>
                    </div>


                </div>
            </div>
        );
    }
}




export default Alert;
