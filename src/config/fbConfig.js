import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

var firebaseConfig = {
    apiKey: "AIzaSyDNqw8hP58yM2wqQs612JEV7ZCdmTVuFhQ",
    authDomain: "restaurant-ractjs.firebaseapp.com",
    databaseURL: "https://restaurant-ractjs.firebaseio.com",
    projectId: "restaurant-ractjs",
    storageBucket: "",
    messagingSenderId: "1093250641221",
    appId: "1:1093250641221:web:599d69d1a810d54c"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

firebase.firestore().settings({ timestampsInSnapshots: true });

export default firebase;